import java.util.Scanner;

public class Cadena{
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite dos cadenas para operar sobre ellas \n");
    	String str = sc.nextLine();
    	String str2 = sc.nextLine();
    	System.out.println("Digite la operacion a realizar");
    	System.out.println("1. Concatenacion");
    	System.out.println("2. Potencia");
    	System.out.println("3. Longitud");
    	System.out.println("4. Reflexion o inversa");
    	System.out.println("5. Subcadenas");
    	System.out.println("6. Prefijos y sufijos");
    	int option = sc.nextInt();
    	
    	switch(option){
    		case 1:
    			System.out.println("Resultado de la concatenacion: " + concat(str, str2));
    			break;
    		case 2:
    			System.out.println("Digite el valor de la potencia: ");
    			int pow = sc.nextInt();
    			System.out.println("Resultado de la potencia: " + stringExponentiation(str, pow));
    			break;
    		case 3:
    			System.out.println("Longitud de la cadena: " + str.length());
    			break;
    		case 4:
    			System.out.println("Cadena invertida: " + reverse(str));
    			break;
    		case 5: 
    			System.out.println("La subcadena " + str2 + subString(str, str2));
    			break;
    		case 6:
    		    System.out.println("La subcadena " + str2 + prefixAndSuffix(str, str2));
    		    break;
    		default:
    		    System.out.println("Error. Opcion invalida");
    		    break;
    	}
    }
    
    public static String concat(String str, String str2){
        return str + str2;
    }
    
    public static String stringExponentiation(String str, int pow) {
        StringBuilder finalStr = new StringBuilder();
        for (int i = 0; i < pow; i++) {
            finalStr.append(str);
        }
        return finalStr.toString(); 
    }
    
    public static String reverse(String str){
        StringBuilder reversedStr = new StringBuilder(str);
        return reversedStr.reverse().toString();
    }
    
    public static String subString(String str, String subStr){
        return (str.contains(subStr) ? " si esta en la cadena " + str : " no la esta en la cadena " + str);
    }
    
    public static String prefixAndSuffix(String str, String subStr){
        String result = "";
        if (str.startsWith(subStr)) {
            result += " es un prefijo de " + str;
        }
        if (str.endsWith(subStr)) {
            result += " es un sufijo de " + str;
        }
        return result;
    }
}